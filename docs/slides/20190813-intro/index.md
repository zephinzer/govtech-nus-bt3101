---
title: Personally Identifiable Information Advisor
separator: <!-- s -->
verticalSeparator: <!-- v -->
revealOptions:
  transition: 'slide'
  center: true
---

## Personally Identifiable Information (PII) Advisor

- - -

Joseph Matthias Goh

<small>Software Engineer, GovTech Singapore</small>

<small><small>(`@zephinzer`)</small></small>

<!-- s -->

# Context & Impact

<!-- v -->

### Intended Scope
MyCareersFuture: Jobs platform

- - -

### Potential Scope
WOG: Your personal information

<!-- v -->

### Possible Scope
TAFEP: Fair employment

<!-- s -->

# Problem Statements

- - -

Questions we ask ourselves

<!-- v -->

**How can we know if/how much of our unstructured data contains personal information?**

<!-- v -->

**How can we integrate with private sector offerings while securing citizens' data?**

<!-- v -->

**How can we classify our data more seamlessly?**

<!-- s -->

# Data

<!-- v -->

### Document Formats

- - -

mostly resumes

<small>.pdf (tricky), .docx, .doc, .odt, .txt</small>

<!-- v -->

### Entities of Interest

- NRIC numbers (social security numbers)
- Phone numbers
- Email addresses
- Physical addresses
- Work experiences (?)
- Race/Religion/Etc?

<!-- v -->

### Operations of Interest

- Flagging out of PIIs
- Removal of PIIs
- Alerting of PIIs
- Metrics on PIIs

<!-- s -->

# Solution

- - -

### Functional Requirements

<!-- v -->

- Flag out any PIIs
- Removal of PIIs
- Alerting administrators of files containing PII
- Generate report of files with PIIs

<!-- s -->

# Solution

- - -

### Interfacing Requirements

<!-- v -->

### Single Scans

- Upload-time scans
- RPC/RESTful interface
- Queue based result retrieval

<!-- v -->

### Single Scan Example

```sh
$ curl -d '$(cat ./file)' http://localhost:12345/scan
# or
$ curl -d '$(cat ./file)' http://localhost:12345/mask

> {"scan_id": 123456}
```

```sh
$ curl http://localhost:12345/results/123456

> {"status": "processing"}
# or...
> {"status": "done", "results": "..."}
```

<!-- v -->

### Cron-based daily scan

- Existing data storage scans
- Configurable webhook for alerts (over HTTPS)
- Report generation (JSON)

<!-- v -->

### Example daily scan report

```json
{
  "files_scanned": 12345,
  "files_flagged": 200,
  "flagged": [
    {
      "path": "...",
      "entities": [ ... ],
      "classification": "confidential",
      "confidence": "0.823"
    }
  ]
}
```

<!-- s -->

# Solution

- - -

### Non-Functional Requirements

<!-- v -->

### Packaging

- Runs in a Docker container
- Eventual deployment in a Kubernetes cluster
- Service provider queue if possible (AWS SQS - free tier: 1 million calls)
- Separate service if needed (RabbitMQ/Redis/etc)

<!-- v -->

### Languages

- (Java|Type)Script
- Golang
- Python
- Java

<!-- v -->

### Deliverables

- Code repository (MIT licensed)
- Documentation (setup, API specification)
- Tests (probably, maybe? if you have enough time!)

<!-- s -->

# "What's in it for me?"

- - -

### Fame, glory, fun, profit?

<!-- v -->

### Challenges

- Defining personally identifiable information
- More reliably understanding unstructured data
- Enabling business process automation through API design

<!-- v -->

### Misc

- Intended to be open sourced (read: portfolio)
- Reusable across government (read: legacy)
- Learn modern deployment software like Kubernetes (read: graduate marketability)
- I'm actually an engineer too (read: industry experience advice)

<!-- s -->

# Thank you

- - -

questions?

